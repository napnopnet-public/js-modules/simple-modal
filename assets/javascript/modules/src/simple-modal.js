//  simple-modal/assets/javascript/modules/src/simple-modal.js
/**
 * Simple Modal class, for very simple alert modals.
 *
 * @author Wouter J. van den Brink/ wowter.napnop.net / @wowter <>
 * @copyright Wouter J. van den Brink 2019
 * @license MIT
 * @see {@link ./LICENSE.md license text}
 *
 */
class SimpleModal {
    /**
     *
     * @param {string} prefix - custom prefix for  html tag  attributes
     * @constructor
     */
    constructor(prefix = "") {

        this.prefix = prefix;

        // here determine if the dialog tag is supported or not (only chrome at this moment?)
        let dialogTag = document.createElement("dialog");

        this.supportsDialog = typeof dialogTag.showModal === "function";
        dialogTag = null;
    }

    /**
     * Display modal with a message text with some allowed html tags, a close button, and on close perform a function that is provided as parameter.
     *
     * @param {string} message - the text message for the modal.
     * @param {string} allowed_tags - string with allowed html tags like "<br>,<b>"  to allow (br and b) tags.
     * @param {string} confirmText - text for confirm/close button.
     * @param {function} runThis - function that is run when confirm/close button is clicked and modal is closed.
     */
    showMessage(message = "I forgot what to say:-(", allowed_tags = "", confirmText = "X", runThis = function () {
    }) {

        // todo: find a way to halt javascript execution progress and implement parameter wait==true

        const cleanedMessage = this.strip_tags(message, allowed_tags);

        if (this.supportsDialog === true) {

            let dialogTag = document.createElement("dialog");
            dialogTag.classList.add(this.prefix + "dialog");

            let dialogTagStyle = document.createElement("style");
            dialogTagStyle.innerHTML = `
            dialog.${this.prefix}dialog {
                  font-size: calc(2*calc(1vw + 1vh));
                  box-sizing: border-box;
                  position: fixed;
                  left: 50%;
                  top: 50%;
                  transform: translate(-50%, -50%);
                  border-style: solid;
                  border-width: 0.1em;
                  border-radius: 1em;
                  padding:1em;
                  max-width: 90vw;
                  max-height: 90vh;
                  overflow: auto;
              }
              dialog.${this.prefix}dialog button {
                  font-size:inherit;
              }
              dialog.${this.prefix + "dialog"}::backdrop {
                 background-color: orange;
                 opacity: 0.3;
                 position:fixed;
                 top:0px;
                 left:0px;
                 bottom:0px;
                 right:0px;
              }
            `;
            dialogTag.prepend(dialogTagStyle);

            let messageDiv = document.createElement('div');
            messageDiv.style.textAlign = "center";
            messageDiv.innerHTML = cleanedMessage;

            let confirmButton = document.createElement('button');
            let confirmButtonText = document.createTextNode(confirmText);
            confirmButton.appendChild(confirmButtonText);
            confirmButton.style.cssFloat = "right";
            confirmButton.addEventListener("click", function () {
                dialogTag.close();
                dialogTag.remove();
                runThis();
            });

            dialogTag.appendChild(messageDiv);
            dialogTag.appendChild(confirmButton);
            let thisBody = document.getElementsByTagName('body')[0];
            thisBody.appendChild(dialogTag);
            dialogTag.showModal();
        } else {
            // see https://stackoverflow.com/questions/491052/minimum-and-maximum-value-of-z-index
            // use 2147483647 for highest z-index, 2147483647 for backdrop
            let dialogTag = document.createElement("div");
            // let innerDiv = document.createElement("div");
            let backdrop = document.createElement("div");

            dialogTag.classList.add(this.prefix + "dialog");
            backdrop.classList.add(this.prefix + "dialog-backdrop");

            let dialogTagStyle = document.createElement("style");

            dialogTagStyle.innerHTML = `
              div.${this.prefix}dialog-backdrop  {
                  background-color: orange;
                  opacity: 0.3;
                  position:fixed;
                  top:0px;
                  left:0px;
                  bottom:0px;
                  right:0px;
                  z-index:2147483646;
              }
              
              div.${this.prefix}dialog {
                  font-size: calc(2*calc(1vw + 1vh));
                  background-color: white;
                  z-index:2147483647;
                  min-width: 200px;
                  padding: 1em;
                  position: fixed;
                  left: 50%;
                  top: 50%;
                  transform: translate(-50%, -50%);
                  border-style: solid;
                  border-width: 0.1em;
                  border-radius: 1em;
                  box-sizing: border-box;
                  max-width: 90vw;
                  max-height: 90vh;
                  overflow: auto;
              }
              
              div.${this.prefix}dialog button {
                 margin-right:1em;
                 margin-bottom: 1em;
                 border-radius: 0.3em;
                 font-size: inherit;
              }
            `;
            dialogTag.prepend(dialogTagStyle);

            let messageDiv = document.createElement('div');
            messageDiv.style.textAlign = "center";
            messageDiv.innerHTML = cleanedMessage;

            let confirmButton = document.createElement('button');
            let confirmButtonText = document.createTextNode(confirmText);
            confirmButton.appendChild(confirmButtonText);
            confirmButton.style.cssFloat = "right";
            confirmButton.addEventListener("click", function () {
                dialogTag.style.display = "none";
                backdrop.style.display = "none";
                dialogTag.remove();
                backdrop.remove();
                runThis();
            });

            dialogTag.appendChild(messageDiv);
            dialogTag.appendChild(confirmButton);

            let thisBody = document.getElementsByTagName('body')[0];
            thisBody.appendChild(backdrop);
            thisBody.appendChild(dialogTag);
        }
    }

    /**
     * Strip html tags while allowing some
     *
     * Copied from https://raw.githubusercontent.com/hirak/phpjs/master/functions/strings/strip_tags.js
     * form https://github.com/hirak/phpjs
     * Copyright (c) 2013 Kevin van Zonneveld (http://kvz.io) and Contributors (http://phpjs.org/authors)
     * @license MIT
     * @see {@link ./LICENSE.txt license text}
     *
     *
     * @param {string} input
     * @param {string} allowed
     * @returns {string}
     */
    strip_tags(input, allowed) {
        //  discuss at: http://phpjs.org/functions/strip_tags/
        // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // improved by: Luke Godfrey
        // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        //    input by: Pul
        //    input by: Alex
        //    input by: Marc Palau
        //    input by: Brett Zamir (http://brett-zamir.me)
        //    input by: Bobby Drake
        //    input by: Evertjan Garretsen
        // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // bugfixed by: Onno Marsman
        // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // bugfixed by: Eric Nagel
        // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // bugfixed by: Tomasz Wesolowski
        //  revised by: Rafał Kukawski (http://blog.kukawski.pl/)
        //   example 1: strip_tags('<p>Kevin</p> <br /><b>van</b> <i>Zonneveld</i>', '<i><b>');
        //   returns 1: 'Kevin <b>van</b> <i>Zonneveld</i>'
        //   example 2: strip_tags('<p>Kevin <img src="someimage.png" onmouseover="someFunction()">van <i>Zonneveld</i></p>', '<p>');
        //   returns 2: '<p>Kevin van Zonneveld</p>'
        //   example 3: strip_tags("<a href='http://kevin.vanzonneveld.net'>Kevin van Zonneveld</a>", "<a>");
        //   returns 3: "<a href='http://kevin.vanzonneveld.net'>Kevin van Zonneveld</a>"
        //   example 4: strip_tags('1 < 5 5 > 1');
        //   returns 4: '1 < 5 5 > 1'
        //   example 5: strip_tags('1 <br/> 1');
        //   returns 5: '1  1'
        //   example 6: strip_tags('1 <br/> 1', '<br>');
        //   returns 6: '1 <br/> 1'
        //   example 7: strip_tags('1 <br/> 1', '<br><br/>');
        //   returns 7: '1 <br/> 1'

        allowed = (((allowed || '') + '')
            .toLowerCase()
            .match(/<[a-z][a-z0-9]*>/g) || [])
            .join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
        var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
            commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
        return input.replace(commentsAndPhpTags, '')
            .replace(tags, function($0, $1) {
                return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
            });
    }

}

export { SimpleModal }
